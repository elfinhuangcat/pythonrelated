PythonRelated
===========

###What's in this repo:

**Under the directory "AI_assign_proj":**

* Assignment 1 : A simple tic-tac-toe game, where the AI will never lose.
* Assignment 2 : A solution for the wumpus-world. Currently it is not a good one.
* Poker-Game : The course project I did with Matthew, Michael and Rui. I only include what I made.

**Under the directory "AlgorithmicThinking_Coursera":**

All the stuff I think valuable to keep it here.

* CodingGuidelines: Currently includes the coding style guidelines.